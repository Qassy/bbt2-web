﻿<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.vb" Inherits="web._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>BITMAN-3B - BSYS 3105 - Team 2</h1>
        <p class="lead">Marco Canil, Marcus Lee, Tyler Toppazzini, Sandy Chan, Jun Choi</p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Create, Display, and Modify Work Order</h2>
            <p>
                <a class="btn btn-default" href="./CDM.aspx">Go &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Display Parts</h2>
            <p>
                <a class="btn btn-default" href="./DP.aspx">Go &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Check Inventory and Update</h2>
            <p>
                <a class="btn btn-default" href="./CIU.aspx">Go &raquo;</a>
            </p>
        </div>
                <div class="col-md-4">
            <h2>Issue Raw Material</h2>
            <p>
                <a class="btn btn-default" href="./IRM.aspx">Go &raquo;</a>
            </p>
        </div>
                <div class="col-md-4">
            <h2>Produce and Post Finished Products</h2>
            <p>
                <a class="btn btn-default" href="./PPFP.aspx">Go &raquo;</a>
            </p>
        </div>
    </div>

</asp:Content>
