﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CDM.aspx.vb" Inherits="web.CDM" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            &lt;Create a New Work Order&gt;<br />
            <br />
            <br />
            Product ID :
            <asp:TextBox ID="ProductID" runat="server"></asp:TextBox>
            <br />
            <br />
            Order Quantity :
            <asp:TextBox ID="OrderQuantity" runat="server"></asp:TextBox>
            <br />
            <br />
            Scrapped Quantity :
            <asp:TextBox ID="ScrappedQty" runat="server"></asp:TextBox>
            <br />
            <br />
            Start Date :
            <asp:TextBox ID="StartDate" runat="server"></asp:TextBox>
            <br />
            <br />
            End Date :
            <asp:TextBox ID="EndDate" runat="server"></asp:TextBox>
            <br />
            <br />
            Due Date :<asp:TextBox ID="DueDate" runat="server"></asp:TextBox>
            <br />
            <br />
            <br />
            <asp:Button ID="Button1" runat="server" Text="Create" />
            <br />
            <br />
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="WorkOrderID" DataSourceID="SqlDataSource1">
                <Columns>
                    <asp:BoundField DataField="WorkOrderID" HeaderText="WorkOrderID" InsertVisible="False" ReadOnly="True" SortExpression="WorkOrderID" />
                    <asp:BoundField DataField="OrderQty" HeaderText="OrderQty" SortExpression="OrderQty" />
                    <asp:BoundField DataField="StockedQty" HeaderText="StockedQty" ReadOnly="True" SortExpression="StockedQty" />
                    <asp:BoundField DataField="ScrappedQty" HeaderText="ScrappedQty" SortExpression="ScrappedQty" />
                    <asp:BoundField DataField="StartDate" HeaderText="StartDate" SortExpression="StartDate" />
                    <asp:BoundField DataField="EndDate" HeaderText="EndDate" SortExpression="EndDate" />
                    <asp:BoundField DataField="DueDate" HeaderText="DueDate" SortExpression="DueDate" />
                    <asp:BoundField DataField="ScrapReasonID" HeaderText="ScrapReasonID" SortExpression="ScrapReasonID" />
                    <asp:BoundField DataField="ModifiedDate" HeaderText="ModifiedDate" SortExpression="ModifiedDate" />
                    <asp:BoundField DataField="ProductID" HeaderText="ProductID" SortExpression="ProductID" />
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:JunCD %>" SelectCommand="CreateANewWorkOrder" SelectCommandType="StoredProcedure">
                <SelectParameters>
                    <asp:ControlParameter ControlID="ProductID" Name="ProductID" PropertyName="Text" Type="Int32" />
                    <asp:ControlParameter ControlID="ScrappedQty" Name="ScrappedQty" PropertyName="Text" Type="Int16" />
                    <asp:ControlParameter ControlID="OrderQuantity" Name="OrderQty" PropertyName="Text" Type="Int32" />
                    <asp:ControlParameter ControlID="StartDate" Name="StartDate" PropertyName="Text" Type="DateTime" />
                    <asp:ControlParameter ControlID="EndDate" Name="EndDate" PropertyName="Text" Type="DateTime" />
                    <asp:ControlParameter ControlID="DueDate" Name="DueDate" PropertyName="Text" Type="DateTime" />
                </SelectParameters>
            </asp:SqlDataSource>
            <br />
        </div>


    <div style="height: 476px">

        &lt;Modify Your Order&gt;<br />
        <br />
        Work Order ID :
        <asp:TextBox ID="MWorkOrderID" runat="server"></asp:TextBox>
        <asp:SqlDataSource ID="WOID" runat="server" ConnectionString="<%$ ConnectionStrings:JunCD %>" SelectCommand="CheckWorkOrderID" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        <br />
        <br />
        Product: <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="PIDrop" DataTextField="Name" DataValueField="ProductID">
        </asp:DropDownList>
        <asp:SqlDataSource ID="PIDrop" runat="server" ConnectionString="<%$ ConnectionStrings:JunCD %>" SelectCommand="CheckProductID" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        <br />
        <br />
        Order Quantity :
        <asp:TextBox ID="MOrderQuantity" runat="server"></asp:TextBox>
        <br />
        <br />
        Due Date : <asp:TextBox ID="MDueDate" runat="server"></asp:TextBox>
        <br />
        <br />

        <asp:Button ID="Button2" runat="server" Text="Modify" />

        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" DataKeyNames="WorkOrderID" DataSourceID="ModifyGrid">
            <Columns>
                <asp:BoundField DataField="WorkOrderID" HeaderText="WorkOrderID" InsertVisible="False" ReadOnly="True" SortExpression="WorkOrderID" />
                <asp:BoundField DataField="OrderQty" HeaderText="OrderQty" SortExpression="OrderQty" />
                <asp:BoundField DataField="StockedQty" HeaderText="StockedQty" ReadOnly="True" SortExpression="StockedQty" />
                <asp:BoundField DataField="ScrappedQty" HeaderText="ScrappedQty" SortExpression="ScrappedQty" />
                <asp:BoundField DataField="StartDate" HeaderText="StartDate" SortExpression="StartDate" />
                <asp:BoundField DataField="EndDate" HeaderText="EndDate" SortExpression="EndDate" />
                <asp:BoundField DataField="DueDate" HeaderText="DueDate" SortExpression="DueDate" />
                <asp:BoundField DataField="ScrapReasonID" HeaderText="ScrapReasonID" SortExpression="ScrapReasonID" />
                <asp:BoundField DataField="ModifiedDate" HeaderText="ModifiedDate" SortExpression="ModifiedDate" />
                <asp:BoundField DataField="ProductID" HeaderText="ProductID" SortExpression="ProductID" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="ModifyGrid" runat="server" ConnectionString="<%$ ConnectionStrings:JunCD %>" SelectCommand="ModifyWorkOrder" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="DropDownList1" Name="ProductID" PropertyName="SelectedValue" Type="Int32" />
                <asp:ControlParameter ControlID="MWorkOrderID" Name="WorkOrderID" PropertyName="Text" Type="Int32" />
                <asp:ControlParameter ControlID="MOrderQuantity" Name="OrderQty" PropertyName="Text" Type="Int32" />
                <asp:ControlParameter ControlID="MDueDate" Name="Duedate" PropertyName="Text" Type="DateTime" />
            </SelectParameters>
        </asp:SqlDataSource>

    </div>
    </form>
</body>
</html>
