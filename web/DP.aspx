﻿<%@ Page Title="Display Parts" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DP.aspx.vb" Inherits="web.DP" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        #form1 {
            text-align: center;

        }
        #Submit1 {
            height: 35px;
        }
       
      
         .newStyle2 {
            margin-top: 1rem;
            margin-right: 1rem;
        }
          .newStyle3 {
            margin-top: 1rem;
            margin-right: 1rem;
            text-align:center;
            margin-bottom:18rem;
           
        
       

    </style>
<div id="form1">
        <asp:GridView ID="GridView1" runat="server" DataSourceID="SqlDataSource1" AutoGenerateColumns="False" Height="283px" Width="100%" style=" margin-top:10rem; margin-bottom:5rem;" EmptyDataText="Please Select An Assembly ID &amp; Start Date">
            <Columns>
                <asp:BoundField DataField="ProductAssemblyID" HeaderText="ProductAssemblyID" ReadOnly="True" SortExpression="ProductAssemblyID" />
                <asp:BoundField DataField="ComponentID" HeaderText="ComponentID" ReadOnly="True" SortExpression="ComponentID" />
                <asp:BoundField DataField="ComponentDesc" HeaderText="ComponentDesc" ReadOnly="True" SortExpression="ComponentDesc" />
                <asp:BoundField DataField="TotalQuantity" HeaderText="TotalQuantity" ReadOnly="True" SortExpression="TotalQuantity" />
                <asp:BoundField DataField="StandardCost" HeaderText="StandardCost" ReadOnly="True" SortExpression="StandardCost" />
                <asp:BoundField DataField="ListPrice" HeaderText="ListPrice" ReadOnly="True" SortExpression="ListPrice" />
                <asp:BoundField DataField="BOMLevel" HeaderText="BOMLevel" ReadOnly="True" SortExpression="BOMLevel" />
                <asp:BoundField DataField="RecursionLevel" HeaderText="RecursionLevel" ReadOnly="True" SortExpression="RecursionLevel" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:AdventureWorks2014GetBillOfMaterials %>" SelectCommand="uspGetBillOfMaterials" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="DropDownList1" Name="StartProductID" PropertyName="SelectedValue" Type="Int32" />
                <asp:ControlParameter ControlID="DropDownList2" Name="CheckDate" PropertyName="SelectedValue" Type="DateTime" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:AdventureWorks2014GetBillOfMaterials %>" SelectCommand="CheckBillofMaterialsID" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:AdventureWorks2014GetBillOfMaterials %>" SelectCommand="CheckBillofMaterialsDate" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        <br />
    <div id="formStyle">  
        <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSource2" DataTextField="ProductAssemblyID" DataValueField="ProductAssemblyID" style="margin-left: 69px" CssClass="newStyle1">
        </asp:DropDownList>
&nbsp;&nbsp;&nbsp; Product Assembly ID<br />
        <br />
        <asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="SqlDataSource3" DataTextField="StartDate" DataValueField="StartDate" CssClass="newStyle2">
        </asp:DropDownList>
&nbsp;&nbsp;&nbsp; Start Date<br />
        <br />
        
        <div class="btn-group">
            <input id="Submit1" type="submit" value="Submit" class="newStyle3 btn btn-default" /> 
            <a class="btn btn-default newStyle3"" href="./Default.aspx">Home</a>
            </div>
        <br />
        <br />

    </div>
      
</div>

</asp:Content>
