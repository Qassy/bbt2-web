﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="IRM.aspx.vb" Inherits="web.IRM" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            font-size: x-large;
            font-weight: bold;
            font-family: "Calibri Light";
        }
        .auto-style2 {
            font-family: "Calibri Light";
            font-weight: bold;
        }
        .body{
            background-color: aliceblue;
        }

    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="auto-style1">
            Issue Raw Material</div>
        <br />
        <span class="auto-style2">Product ID:&nbsp;
        <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSource1" DataTextField="Name" DataValueField="ProductID">
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:AdventureWorks2014ConnectionString2 %>" SelectCommand="CheckProductID" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </span>
        <div>
            <br />
            Location ID:<asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="IssueRawMaterials" DataTextField="LocationID" DataValueField="LocationID">
            </asp:DropDownList>
            <asp:SqlDataSource ID="IssueRawMaterials" runat="server" ConnectionString="<%$ ConnectionStrings:JunCD %>" SelectCommand="checklocationID" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
&nbsp;<br />
        </div>
        <span class="auto-style2">Shelf:&nbsp;
        </span>
        <asp:TextBox ID="ShelfTextBox" runat="server" CssClass="auto-style2"></asp:TextBox>
        <div>
            <br />
            Bin:
            <asp:TextBox ID="BinTextbox" runat="server"></asp:TextBox>
            <br />
            <br />
        </div>
        <span class="auto-style2">Quantity:</span>&nbsp;
        <asp:TextBox ID="QuantityTextbox" runat="server"></asp:TextBox>
        <br />
        <br />
        <p>
            <asp:Button ID="Button1" runat="server" Text="Submit" style="font-family: 'Calibri Light'; font-weight: 700" />
        </p>
        <p>
            &nbsp;</p>
        <p>
            &nbsp;</p>
    </form>
</body>
</html>

